/***************************************************************
 * Name:      taskdelegate.cpp
 * Purpose:   Defines the TaskDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2020-12-05
 * Copyright: JacksiroKe (https://github.com/JacksiroKe)
 * License:
 **************************************************************/

#ifndef TASKDELEGATE_H
#define TASKDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QStandardItemModel>

class TaskDelegate : public QStyledItemDelegate
{
    Q_OBJECT
        signals :

public:
    explicit TaskDelegate(QObject* parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

    ~TaskDelegate();
};

#endif // TASKDELEGATE_H
