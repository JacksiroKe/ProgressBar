/***************************************************************
 * Name:      mainwindow.cpp
 * Purpose:   Defines the Main Window class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2020-12-05
 * Copyright: JacksiroKe (https://github.com/JacksiroKe)
 * License:
 **************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

public slots:

private slots:
    void loadTasks();

};

#endif // MAINWINDOW_H
