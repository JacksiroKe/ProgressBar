/***************************************************************
 * Name:      mainwindow.cpp
 * Purpose:   Implements the Main Window class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2020-12-05
 * Copyright: JacksiroKe (https://github.com/JacksiroKe)
 * License:
 **************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <taskitem.h>
#include <taskdelegate.h>
#include <QStandardItemModel>
#include <QObject>

using namespace std;

TaskDelegate *taskDelegate;
QStandardItemModel *itemModel;
vector<vector<QString>> tasks;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    
	itemModel = new QStandardItemModel();
	taskDelegate = new TaskDelegate(this);

	ui->LstTasks->setItemDelegate(taskDelegate);
	ui->LstTasks->setModel(itemModel);
	ui->LstTasks->setSpacing(1);

    loadTasks();
}

void MainWindow::loadTasks()
{
    tasks.push_back({"Indefinate Task","d6b067593c57ad89fc44f8fc79d36a24", "-1", "4 seconds"});
    tasks.push_back({"Doing stuff in update","Baking... a pie", "78", "8 seconds"});
    tasks.push_back({"Default Remaining Time","Doing random stuff", "7", "2 minutes left"});
    tasks.push_back({"This will be unresponsive","Downloading data with a modem (Not Responding)", "19", "4 seconds"});
    tasks.push_back({"Succeed task","Success", "100", "0 seconds"});
    tasks.push_back({"Cancelled task","Cancelled", "50", ""});
    tasks.push_back({"Failed task","Some error occured", "50", ""});
    
    for (decltype(tasks.size()) i = 0; i < tasks.size(); ++i)
    {        
        QStandardItem* qStdItem = new QStandardItem;
        TaskItem taskItem;
        taskItem.title = tasks[i][0];
        taskItem.details = tasks[i][1];
        taskItem.progress = tasks[i][2];
        taskItem.time = tasks[i][3];

        qStdItem->setData(QVariant::fromValue(taskItem), Qt::UserRole + 1);
        itemModel->appendRow(qStdItem);
    }
    QString tasksCountLabel = "Background Tasks (" + QString::number(tasks.size()) + ")";
    this->setWindowTitle(tasksCountLabel);
    ui->TabWidget->setTabText(ui->TabWidget->indexOf(0), "Background Tasks " + QString::number(tasks.size()));
    //ui->TabWidget->setTabText(ui->TabWidget->indexOf(0), QCoreApplication::translate("MainWindow", "Background Processes", nullptr));
}

MainWindow::~MainWindow()
{
    delete ui;
}
