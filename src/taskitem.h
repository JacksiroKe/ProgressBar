/***************************************************************
 * Name:      TaskItem.cpp
 * Purpose:   Defines the TaskItem class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2020-12-05
 * Copyright: JacksiroKe (https://github.com/JacksiroKe)
 * License:
 **************************************************************/

#ifndef TASKITEM_H
#define TASKITEM_H

#include <QMetaType>

typedef enum {
    T_RED,
    T_GREEN,
    T_YELLOW,
} ItemStatus;

struct TaskItem {
    QString title;
    QString details;
    QString progress;
    QString time;
};

Q_DECLARE_METATYPE(TaskItem)

#endif // TASKITEM_H
