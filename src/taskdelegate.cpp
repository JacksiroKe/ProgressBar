/***************************************************************
 * Name:      taskdelegate.cpp
 * Purpose:   Implements the TaskDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2020-12-05
 * Copyright: JacksiroKe (https://github.com/JacksiroKe)
 * License:
 **************************************************************/

#include <taskdelegate.h>
#include <taskitem.h>

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

TaskDelegate::TaskDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

TaskDelegate::~TaskDelegate()
{

}

void TaskDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        TaskItem item = var.value<TaskItem>();

        // item Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width() - 1);
        rect.setHeight(option.rect.height() - 10);

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());

        QRectF itemText1, itemText2, itemText3;

        itemText1 = QRect(rect.left() + 5, rect.top() + 2, rect.width() - 200, 20);
        itemText2 = QRect(rect.left() + 5, itemText1.bottom() - 5, rect.width() - 100, 20);
        itemText3 = QRect(itemText2.right(), itemText1.bottom(), 90, 20);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 10, QFont::Bold));
        painter->drawText(itemText1, item.title);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 8, 0));
        painter->drawText(itemText2, item.details);

        int progress = item.progress.toInt();

        QStyleOptionProgressBar progressBar;
        progressBar.rect = option.rect.adjusted(itemText1.right(), 5, 0, -30);
        progressBar.minimum = 0;
        progressBar.maximum = 100;

        if (progress < 0) {
            progressBar.progress = 10;
        }
        else {
            progressBar.progress = progress;
            progressBar.text = QString::number(progress) + "%";
        }
        progressBar.textVisible = true;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBar, painter);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 8, 0));
        painter->drawText(itemText3, item.time);

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, itemText2.bottom() + 10, rect.width(), itemText2.bottom() + 10);

        painter->restore();
    }
}

QSize TaskDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
